.. image:: https://gitlab.com/lpirl/girafs/badges/master/pipeline.svg
  :align: right
  :target: https://gitlab.com/lpirl/girafs/commits/master

**gi**\ t **r**\ epository **a**\ ccess via **f**\ ile **s**\ ystem
===================================================================

*girafs* is intended to create local clones of git repositories and keep
them in (two-way) sync with the remote.

It can be used it, for example, to allow people not comfortable with
*git* to edit the contents of a repository (maybe the synchronized
repositories are exported as a network share).

If the underlying file system supports **inotify**, commits and pushes
can be triggered by changes in the local clone of a repository (no
polling required).

If a remote repository is hosted on *GitLab*, girafs can set up **Web
hooks** via the GitLab API to receive notifications about remote
updates (no polling required).

As a fall back, pulls and pushes from/to the remote can also be based
on **polling** (i.e., triggered in time intervals). Of course, this is
less efficient and does not scale well in comparison to the
implementations described above.

Please refer to the configuration_ for details.

getting started
---------------

0. create and enter a virtualenv, if desired::

    mkvirtualenv -p /usr/bin/python3 girafs

#. install girafs::

    pip install git+https://gitlab.com/lpirl/girafs.git

   (alternatively, you can of course clone this repository, install the
   requirements specified in `setup.py <setup.py>`__ and run
   ``./girafs``)

#. make sure the binaries installed by pip are in your ``$PATH``::

    export PATH="~/.local/bin:$PATH"

#. to run a small example, execute::

    girafs example.ini

   (quit with ``Ctrl+C``)

#. create your own configuration file (see below)

If you use Ansible,  there is an `Ansible role to deploy girafs
<https://github.com/lpirl/ansible-roles/tree/master/roles/girafs>`__. This role
is more targeted towards deployments rather than a first hands-on, since the
role also installs and configures nginx as a reverse proxy for the girafs
Webhook server etc.

configuration
-------------

All configuration is done in a single INI file.
It's structure and the available options are described below.

sections
........

Per INI file, each configuration section corresponds to a repository
to keep in sync, e.g.::

  [myrepo]
  clone_url = https://git.example.com/example.git

An exception to this is the ``[default]`` section, which provides
defaults for all other sections.
If you want to unset a default option in another section,
just assign an empty value::

  [default]
  …
  push_notifier = InotifyPushNotifier

  [myrepo]
  …
  push_notifier =

  […]

options
.......

Per section, the following options are available.
Some have dependencies between each other (e.g. ``gitlab_*``) which
hopefully is not too opaque. If some dependencies are not met, girafs
will (should) output an error message and quit.

``clone_url`` (required)
  The URL to clone the repository from.

  Specify the URL as you would specify it for ``git clone``
  (``git@example.com/example.git``, ``file:///home/me/my-repo``, …).

``clone_to_dir`` (default: ``~/girafs-repositories/<section-name>``)
  Directory to clone the repository to.

  Just as in ``git clone <url> <clone_to_dir>``.

  The directory must not exist, except it is already a valid clone of
  the repository.

``checkout_ref`` (default: ``master``)
  The named reference (branch or tag name) to check out.

``commit_message`` (default: "automatic commit from <fqdn>:<clone_to_dir>")
  The commit message for committing local changes.

``pull_notifier`` (default: none)
  The implementation which triggers pulls from the remote. Options are:

  * ``IntervalPullNotifier``: Pulls in time intervals (polling).
  * ``GitLabPullNotifier``: Triggers pulls when receiving GitLab Web
    hook requests (no polling).

    This will make girafs start a local Web server to listen for Web
    hook requests issued by GitLab. Because this (i.e., a Python
    `http.server
    <https://docs.python.org/3/library/http.server.html>`__) is not a
    production-grade Web server, it is hard-coded to listen on
    ``localhost`` only. Please configure a proper Web server as a
    reverse proxy.

  See options below for more information.

``push_notifier`` (default: none)
  The implementation which triggers (commits and) pushes to the remote.
  Options are:

  * ``IntervalPushNotifier``: Pushes in time intervals (polling).
  * ``InotifyPushNotifier``: Pushes upon changes in the local clone
    (no polling).

``interval_pull_secs``/``interval_push_secs`` (default: ``100``)
  When using the ``IntervalPullNotifier``/``IntervalPushNotifier``,
  the amount of seconds after which a pull/push is triggered.

``interval_pull_random_offset_secs``/``interval_push_random_offset_secs`` (default: 20 % of pull/push interval)
  When using the ``IntervalPullNotifier``/``IntervalPushNotifier``,
  the amount of seconds the pull/push interval is being randomized.

  For every iteration, the interval will be randomly chosen from the
  range ``[interval - offset/2; interval + offset/2]``.

  This is intended to avoid the thundering herd problem when you have
  configured many repositories.

``inotify_commit_wait_secs`` (default: ``10``)
  When using the ``InotifyPushNotifier``, the amount of seconds to wait
  for no further change in the repository clone to happen before
  committing and pushing.

  This is intended to avoid heaps of separate commits for changes made
  in quick succession (because the changes probably belong together).

``gitlab_web_hook_token`` (default: random string)
  When using the ``GitLabPullNotifier``, the token to expect in Web hook
  requests sent by GitLab.

  If girafs sets up the Web hooks in GitLab via the GitLab API,
  a random token will be used if this option is not set.

  If you set up the Web hooks in GitLab manually, provide the token
  configured there.

``gitlab_web_hook_server_port`` (default: ``8080``)
  When using the ``GitLabPullNotifier``, the port on which girafs
  listens for Web hook requests.

``gitlab_manage_web_hooks`` (required)
  When using the ``GitLabPullNotifier``, whether or not girafs should
  set up the GitLab Web hooks via the GitLab API on startup (and remove
  them on shutdown).

.. |hook requirement| replace:: Required when using the
  ``GitLabPullNotifier`` and having ``gitlab_manage_web_hooks`` enabled.

``gitlab_url`` (default: none)
  The URL to the GitLab instance.

  |hook requirement|

``gitlab_access_token`` (default: none)
  The access token for the GitLab API.

  |hook requirement|

``gitlab_web_hook_base_url`` (required)
  The URL where Web hook requests from GitLab should be sent to.

  Normally, this is the external host name / IP address of the host
  running girafs.

  |hook requirement|

variables
.........

The configuration file supports
`basic interpolation <https://docs.python.org/3.7/library/configparser.html#configparser.BasicInterpolation>`__.
Additionally, the following variables are available:

``%(section_name)s``
  name of the current section of the INI file, example::

    [myrepo]
    clone_url = https://git.example.com/%(section_name)s.git

  This sets the ``clone_url`` to ``https://git.example.com/myrepo.git``.

``%(host_name)s``
  Host name of the machine as reported by the operating system.
  If, for example, your FQDN is ``myhost.example.com``, this would
  expand to ``myhost``.

``%(fqdn)s``
  Fully-qualified domain name as reported by the operating system.

``%(user)s``
  The user name under which girafs is being executed.

``%(home)s``
  The home directory of the user under which girafs is being executed.

hacking
-------

Issues, questions, and PRs welcome.

There are also some rudimentary
`developer docs <https://lpirl.gitlab.io/girafs>`__
(reflect master branch only).
