from os.path import join as path_join, dirname
from setuptools import setup, find_packages

version = '0.1'
README = path_join(dirname(__file__), 'README.rst')
long_description = open(README).read()
setup(
    name='girafs',
    version=version,
    description=('Git Repository Access via File System'),
    long_description=long_description,
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
    ],
    author='Lukas Pirl',
    author_email='girafs@lukas-pirl.de',
    url='https://gitlab.com/lpirl/girafs',
    download_url='https://gitlab.com/lpirl/girafs/-/archive/master/girafs-master.tar.bz2',
    packages=(['girafs'] +
              ['girafs.%s' % p for p in find_packages(where='lib')]),
    package_dir={'girafs': 'lib'},
    install_requires = [
        'importscan',
        'inotify',
        'gitpython',
        'python-gitlab',
        'singleton-decorator',
    ],
    extras_require={
        'dev': [
            'pylint',
        ],
        'doc': [
            'Sphinx',
        ],
    },
    entry_points={
        'console_scripts': ['girafs = girafs.cli:Cli']
    },
)
