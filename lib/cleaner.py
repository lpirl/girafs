'''
This module contains a class to queue clean up tasks.
'''

from collections import deque
from logging import debug

class Cleaner():
    '''
    A sort of a job queue that holds jobs which should be run (in order)
    before the program exits.
    '''

    def __init__(self):
        ''' initializes instance variables '''
        self._jobs = deque()

    def add_job(self, func, *args, **kwargs):
        ''' add a job to the queue '''
        self._jobs.append((func, args, kwargs))

    def do_all_jobs(self):
        ''' do (and remove) all the jobs in (from) the queue '''
        debug('cleanup: doing all jobs')
        while self._jobs:
            self.do_one_job()
        debug('cleanup: all jobs done')

    def do_one_job(self):
        ''' do and remove one job from the queue '''
        # in reverse order:
        func, args, kwargs = self._jobs.pop()
        debug(f'cleanup: %r, args=%r, kwargs=%r', func, args, kwargs)
        func(*args, **kwargs)
