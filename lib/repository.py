'''
See :class:`Repository`
'''

from getpass import getuser
from logging import error, info, warning, debug
from os import path, makedirs
from pathlib import Path
from socket import getfqdn
from threading import RLock

from git import Repo
from git.config import GitConfigParser, get_config_path
from gitdb.exc import BadName

from .cli import Cli

class Repository:
    '''
    Our abstraction of a git repository.

    Must be thread-safe.
    '''

    @staticmethod
    def check_git_config():
        '''
        Even for local merges, git sometimes requires a commit, so a
        user and an email address has to be set. Let's check the
        configuration values required and set some values if absent.
        '''
        debug(f'checking git configuration for commits')
        with GitConfigParser(get_config_path('global'),
                             read_only=False) as config:
            if not config.get_value('user', 'name', ''):
                name = getuser()
                warning(f'user.name not set in git configuration – '
                        f'falling back to "{name}"')
                config.set_value('user', 'name', name)
            if not config.get_value('user', 'email', ''):
                email = f'{getuser()}@{getfqdn()}'
                warning(f'user.email not set in git configuration – '
                        f'falling back to "{email}"')
                config.set_value('user', 'email', email)


    def __init__(self, name, config, _cleaner):
        '''
        Sets instance variables, loads config, nothing more.
        '''
        info(f'initializing repository {name}')
        self.name = name
        if path.sep in self.name:
            error(f'section "{name}": '
                  f'please do not include any "{path.sep}" characters')
            Cli.exit_due_to_error()

        self.clone_url = config.getstr_or_err('clone_url')
        self.checkout_ref = config.getstr('checkout_ref', 'master')
        self.clone_to_dir = config.getstr(
            'clone_to_dir',
            path.join(str(Path.home()), 'girafs-repositories', name)
        )
        self.commit_message = config.getstr(
            'commit_message',
            f'automatic commit from {getfqdn()}:{self.clone_to_dir}'
        )

        self.lock = RLock()
        self.repo = None

        with self.lock:
            self._setup_repo()


    def _setup_repo(self):
        '''
        Clones repository from remote or – if the directory to clone to
        already exists – re-uses local clone of repository.
        Checks out the named reference as requested.
        '''

        if path.isdir(self.clone_to_dir):
            self.repo = Repo(self.clone_to_dir)
            debug(f'{self} from {self.clone_url} appears to be '
                  f'already cloned to {self.clone_to_dir}')
            self.sync_from_remote()
        else:
            info(f'cloning {self} from {self.clone_url} '
                 f'to {self.clone_to_dir}')
            makedirs(self.clone_to_dir, exist_ok=True)
            self.repo = Repo.clone_from(self.clone_url,
                                        self.clone_to_dir)

        prefixes = ('', 'tags/') + tuple(
            f'{r.name}/' for r in self.repo.remotes
        )
        for prefix in prefixes:
            try:
                full_ref = f'{prefix}{self.checkout_ref}'
                debug(f'trying to check out ref "{full_ref}" in {self}')
                self.repo.git.checkout(full_ref, B=self.checkout_ref)
            except:
                continue
            else:
                break
        else:
            error(f'ref "{self.checkout_ref}" to check out in {self} '
                  f'not found')
            Cli.exit_due_to_error()


    def sync_from_remote(self):
        '''
        Fetches and merges commits from the remote.
        '''
        info(f'pull {self}')
        with self.lock:
            dirty_before_pull = self.repo.is_dirty(untracked_files=True)
            try:
                if dirty_before_pull:
                    self.repo.git.stash(('push', '--all'))
                self.repo.remote().pull(X='theirs')
            except BadName as exception:
                warning(f'{exception} – could be due to a force push '
                        'to the remote')
            if dirty_before_pull:
                self.repo.git.stash('apply')
                self.repo.git.stash('drop')


    def sync_to_remote(self):
        '''
        Commits and pushes local changes to the remote.
        '''
        debug(f'push for {self} requested')
        with self.lock:
            debug(f'checking if need to add/commit/push changes in {self}')
            if self.repo.is_dirty(untracked_files=True):
                info(f'commit and push {self}')
                self.repo.git.add(all=True)
                self.repo.index.commit(self.commit_message)
                self.repo.remote().push()


    def __str__(self):
        return f'repository "{self.name}"'
