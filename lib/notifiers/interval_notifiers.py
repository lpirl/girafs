'''
This module contains the implementation to sync from and to the remote
of a repository based on an interval.
'''

from abc import ABCMeta
from logging import error
from random import uniform
import sys

from ..cli import Cli
from . import BaseNotifier, NotifierRegistry


class IntervalNotifier(BaseNotifier, metaclass=ABCMeta):
    '''
    Baseclass which allows subclasses to easily implement to run
    something in intervals.
    '''

    CONFIG_OPTION_INFIX = None

    def __init__(self, config, repository, cleaner, shutdown_request):
        '''
        Retrieves and checks configuration values and computes derived
        values.
        '''
        super().__init__(config, repository, cleaner, shutdown_request)

        assert self.CONFIG_OPTION_INFIX is not None, \
            'subsclasses must set ``CONFIG_OPTION_INFIX``'

        interval = config.getint(
            f'interval_{self.CONFIG_OPTION_INFIX}_secs', 100
        )
        interval_offset = config.getint_or_err(
            f'interval_{self.CONFIG_OPTION_INFIX}_random_offset_secs',
            fallback=(interval * .2)
        )
        if interval <= interval_offset:
            error(f'"interval_{self.CONFIG_OPTION_INFIX}_secs" '
                  f'({interval}) must be greater than '
                  f'"interval_{self.CONFIG_OPTION_INFIX}_random_offset"'
                  f'_secs ({interval_offset})')
            Cli.exit_due_to_error()

        self.repository = repository
        self.wait = shutdown_request.wait
        self.wait_min = interval - interval_offset // 2
        self.wait_max = interval + interval_offset // 2


    def notify_forever(self):
        '''
        Triggers a sync to the remote repository in the configured
        (possibly randomized) interval. The loop breaks once shutdown
        is requested.
        '''
        while not self.wait(timeout=uniform(self.wait_min, self.wait_max)):
            self._do_notify()

    def _do_notify(self):
        '''
        Needs to be implemented by subclasses to do whatever they want
        to do in the configured interval.
        '''


@NotifierRegistry.register
class IntervalPushNotifier(IntervalNotifier):
    ''' Implements a regular sync to the repository's remote. '''

    CONFIG_OPTION_INFIX = 'push'

    def _do_notify(self):
        ''' notifies the repository to sync to the remote '''
        self.repository.sync_to_remote()


@NotifierRegistry.register
class IntervalPullNotifier(IntervalNotifier):
    ''' Implements a regular sync from the repository's remote. '''

    CONFIG_OPTION_INFIX = 'pull'

    def _do_notify(self):
        ''' notifies the repository to sync from the remote '''
        self.repository.sync_from_remote()
