'''
See :class:`GitLabPullNotifier`.
'''

from json import loads as json_loads
from json.decoder import JSONDecodeError
from logging import error, warning
from urllib.parse import urljoin, urlsplit, quote
from uuid import uuid4

from gitlab import Gitlab

from ..cli import Cli
from . import BaseNotifier, NotifierRegistry
from ..web_server import WebServer


@NotifierRegistry.register
class GitLabPullNotifier(BaseNotifier):
    '''
    Triggers syncs from the remote upon changes on remote GitLab
    repositories by setting up and listening to Web hooks.

    On program termination, the Web hooks are removed again from the
    GitLab project.
    '''

    def __init__(self, config, repository, cleaner, shutdown_request):
        '''
        Retrieves and checks configuration values and tries to do
        everything so that :func:`notify_forever` can work.

        In other words, we will collect all the information we need but
        try not to apply any configuration to the remote GitLab project
        etc.
        '''
        super().__init__(config, repository, cleaner, shutdown_request)

        self.repository = repository
        self.cleaner = cleaner

        # required options
        self.web_hook_server_port = config.getint(
            'gitlab_web_hook_server_port', 8080)
        # should always end with slash, otherwise urljoin strips the
        # last component:
        self.web_hook_base_url = config.get_or_err(
            'gitlab_web_hook_base_url'
        ) + '/'

        self.manage_web_hooks = config.getboolean_or_err(
            'gitlab_manage_web_hooks'
        )

        if not self.manage_web_hooks:
            self.web_hook_token = config.get_or_err(
                'gitlab_web_hook_token'
            )
        else:
            self.web_hook_token = config.get(
                'gitlab_web_hook_token', str(uuid4())
            )

            # get project name/path with name space
            gitlab = Gitlab(
                config.get_or_err('gitlab_url'),
                private_token=config.get_or_err('gitlab_access_token')
            )


        clone_path = urlsplit(self.repository.clone_url)[2]
        project_name = clone_path.rsplit('/', 1)[1].rsplit('.', 1)[0]
        for project in gitlab.projects.list(search=project_name):
            if (project.ssh_url_to_repo == self.repository.clone_url or
                    project.http_url_to_repo == self.repository.clone_url):
                self.gitlab_project = project
                break
        else:
            error(f'did not find GitLab project with clone url '
                  f'"{self.repository.clone_url}" (searched among '
                  f'projects named "{project_name}")')
            Cli.exit_due_to_error()

    def notify_forever(self):
        '''
        Wires our handler (which triggers syncs from the remote) to an
        URL on the Web server and sets up Web hooks in the GitLab
        project.

        The Web server will be shut down by the cleaner, we don't have
        to care (except to tell the cleaner to remove the hook from the
        GitLab project).
        '''
        if self.manage_web_hooks:
            web_hook_url = urljoin(self.web_hook_base_url,
                                   quote(self.repository.name))
            web_hook_path = urlsplit(web_hook_url)[2]
            # make sure hook exists
            for hook in self.gitlab_project.hooks.list():
                if hook.url == web_hook_url:
                    # we need to delete any pre-existing hooks because
                    # the token probably has changed (and GitLab won't
                    # tell the token of a pre-existing hook)
                    hook.delete()
            hook = self.gitlab_project.hooks.create({
                'url': web_hook_url,
                'push_events': 1,
                'token': self.web_hook_token,
            })
            self.cleaner.add_job(hook.delete)

        web_server = WebServer(self.cleaner)
        web_server.subscribe('POST', web_hook_path,
                             self.handle_web_hook_request,
                             port=self.web_hook_server_port)

    def handle_web_hook_request(self, headers, body):
        '''
        Handles requests to the subscribed (Web hook) URL.
        When all checks succeed, this method triggers a sync from the
        remote repository.
        '''
        if headers['X-Gitlab-Token'] != self.web_hook_token:
            return 401
        try:
            parsed_body = json_loads(body)
        except JSONDecodeError as exception:
            warning(f'cannot load json from request ({exception})')
            return 403
        if parsed_body['project_id'] != self.gitlab_project.id:
            warning(f'wrong project ID in request ( '
                    f'got {parsed_body["project_id"]}, '
                    f'expected {self.gitlab_project.id})')
            return 403
        if parsed_body['object_kind'] != 'push':
            warning(f'Web hook not of kind "push"'
                    f'(got "{parsed_body["object_kind"]}")')
            return 403
        self.repository.sync_from_remote()
        return 200
