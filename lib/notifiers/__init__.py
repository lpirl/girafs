'''
This module contains code which is relevant for all notifiers.

Notifiers are responsible for notifying when a pull from or a push to
the remote is necessary.
'''

import sys
from abc import ABCMeta
from logging import info

from importscan import scan



class BaseNotifier(metaclass=ABCMeta):
    '''
    Base class for all notifiers.
    '''

    def __init__(self, config, repository, cleaner, shutdown_request):
        '''
        Set up the notifier and wire up with ``repository``.

        If ``config`` does not configure this notifier, all
        good, ignore silently.
        If ``config`` configures this notifier only partially,
        err and quit.
        '''
        self.repository = repository

    def notify_forever(self):
        '''
        Makes the notifier do its job forever (i.e., until it exits
        due to an error or if until a shutdown is requested).
        '''

    def shutdown(self):
        '''
        Must stop :func:`notify_forever` (if it is not stopping itself
        anyway).
        '''

    def __str__(self):
        return f'{self.__class__.__name__} for {self.repository}'



class NotifierRegistry:
    '''
    Registry where all notifiers have to register.
    See :func:`NotifierRegistry.register` for details.
    '''

    _notifiers = {}

    @classmethod
    def register(cls, notifier_cls):
        '''
        Example usage::

            from lib.notifiers import NotifierRegistry
            @NotifierRegistry.register
            class MyPullNotifier(BaseNotifier):
                …
        '''
        assert notifier_cls.__name__ not in cls._notifiers
        cls._notifiers[notifier_cls.__name__.lower()] = notifier_cls

    @classmethod
    def get(cls, notifier_cls_name):
        '''
        Returns the registered notifier class with the name
        ``notifier_cls_name`` or ``None``, if absent.
        '''
        return cls._notifiers.get(notifier_cls_name.lower(), None)



# loads all submodules, so they (are executed and) can register
scan(sys.modules[__name__])
