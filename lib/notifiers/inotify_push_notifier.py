'''
See :class:`InotifyPushNotifier`.
'''
from logging import debug
from os import walk, unlink, close
from os.path import join
from threading import Lock
from tempfile import mkstemp

from inotify.adapters import InotifyTree
from inotify.constants import IN_ATTRIB, IN_MODIFY

from ..notifiers import BaseNotifier, NotifierRegistry



@NotifierRegistry.register
class InotifyPushNotifier(BaseNotifier):
    '''
    Triggers syncs to the remote upon changes in the file system
    (via inotify events).
    '''

    class InotifyTreeButGitDirs(InotifyTree):
        '''
        Recursively watch a path but ignore any '.git' directories.

        We need this class because it saves us a lot of watches and the
        library itself offers no way to filter the files in a tree to
        watch:
        https://github.com/dsoprea/PyInotify/pull/30
        https://github.com/dsoprea/PyInotify/pull/63
        '''

        def _InotifyTree__load_tree(self, path):
            '''
            Sets up watches for directories except those named '.git'
            and their descendants.

            Really sorry we do this but I can't think of a cleaner way
            right now. See class' comment why we really want to filter
            what is being watched for. Suggestions welcome.
            '''
            for dirpath, subdirs, _f in walk(path):
                if '.git' in subdirs:
                    subdirs.remove('.git')
                self._i.add_watch(dirpath, self._mask)

    def __init__(self, config, repository, cleaner, shutdown_request):
        '''
        Retrieves and checks configuration values and does everything
        to allow this instance to :func:`notify_forever`.
        '''
        super().__init__(config, repository, cleaner, shutdown_request)
        self.cleaner = cleaner
        self.commit_wait = config.getint(
            'inotify_commit_wait_secs',
            10
        )
        self.inotify = self.InotifyTreeButGitDirs(
            self.repository.clone_to_dir,
            mask=(IN_ATTRIB | IN_MODIFY)
        )
        self.git_dir = join(self.repository.clone_to_dir, '.git')
        self.shutdown_request = shutdown_request
        self.lock = Lock()

    def inotify_wait(self, **kwargs):
        '''
        Wait for inotify events. Events very close to each other will
        make this function yield only once.
        '''
        kwargs.setdefault('yield_nones', False)
        # wait for first event to occur:
        for _ in self.inotify.event_gen(**kwargs):
            # clear the events that are queued already (we are not
            # interested in every single specific event
            tuple(self.inotify.event_gen(timeout_s=0))
            yield

    def notify_forever(self):
        '''
        Waits for changes in the file system (i.e., wait for inotify
        events); upon a change, wait for a configured interval within
        no changes happen; then trigger the repository to sync to the
        remote.

        Stops when shutdown is requested (see also
        :func:`wait_for_shutdown`).
        '''

        for _ in self.inotify_wait():
            if self.shutdown_request.is_set():
                break
            debug(f'change in {self.repository} detected – waiting '
                  f'{self.commit_wait} secs before commiting')
            for __ in self.inotify_wait(timeout_s=self.commit_wait):
                if self.shutdown_request.is_set():
                    break
                debug(f'another change in {self.repository} detected – '
                      f'resetting timeout to commit')
                continue
            with self.lock:
                self.repository.sync_to_remote()

    def shutdown(self):
        '''
        This method waits for ``self.shutdown_request`` interrupts
        :func:`inotify_wait`.
        Hence, this method must be run in a separate thread.

        :func:`inotify_wait` is interrupted by creating and deleting a
        temporary file in the root of the repository. An alternative
        implementation would be to wait for inotify events with a
        timeout and check if ``self.shutdown_request`` is set. We prefer
        this implementation to avoid the polling mechanism.'
        '''
        with self.lock:
            descriptor, path = mkstemp(dir=self.repository.clone_to_dir)
            unlink(path)
            close(descriptor)
