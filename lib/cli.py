'''
See :class:`GirafsCli`.
'''

import argparse
from logging import (getLogger, Formatter, StreamHandler, DEBUG, INFO,
                     WARNING, error)
from os import kill, getpid
from signal import signal, SIGTERM, SIGINT, SIGUSR1
import sys


class Cli:
    '''
    Central place for CLI-related code.
    '''

    @staticmethod
    def exit_due_to_error(message=None):
        '''
        Can be used by any thread of the program to terminate the program
        with a non-zero exit code.
        '''
        if message is not None:
            error(message)
        kill(getpid(), SIGUSR1)
        sys.exit()

    def __init__(self):
        '''
        Main CLI entry point and coordination
        '''

        self.setup_parser()

        self.args = self.parser.parse_args()

        self.setup_logger()

        # modules imported by girafs probably want to use
        # :func:`exit_due_to_error` from this class, which is why they
        # need to import this module, which is why we import girafs here
        # to avoid circular imports
        from .girafs import Girafs

        self.girafs = Girafs()

        signal(SIGTERM, self.handle_signal)
        signal(SIGINT, self.handle_signal)
        signal(SIGUSR1, self.handle_signal)

        self.exit_code = 0
        try:
            self.girafs.start(self.args.config_file)
        except Exception as exception:
            self.exit_code = 1
            raise exception
        else:
            self.girafs.shutdown()
        sys.exit(self.exit_code)


    def setup_parser(self):
        '''
        Configures the command line argument parser.
        '''

        self.parser = argparse.ArgumentParser(
            description=('This script enables GIt Repository Access '
                         'via the File System through a local clone. '
                         'Local changes are detected, committed and '
                         'pushed automatically. Remote changes are '
                         'pulled automatically.'),
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )

        self.parser.add_argument('-d', '--debug', action='store_true',
                                 default=False,
                                 help='increase verbosity')
        self.parser.add_argument('-q', '--quiet', action='store_true',
                                 default=False,
                                 help='decrease verbosity')
        self.parser.add_argument('config_file', help='configuration file')


    def setup_logger(self):
        '''
        Configures the logger for the whole program.
        '''

        logger = getLogger()
        logger.name = ''
        formatter_string = '%(levelname)s %(message)s'

        if self.args.quiet:
            logger.setLevel(WARNING)
        elif self.args.debug:
            logger.setLevel(DEBUG)
            formatter_string += ' [%(threadName)s]'
        else:
            logger.setLevel(INFO)

        formatter = Formatter(formatter_string)
        handler = StreamHandler()
        handler.setFormatter(formatter)
        logger.addHandler(handler)


    def handle_signal(self, signum, _frame):
        '''
        Signal handler for interrupts; make the program shut down.
        '''
        if signum == SIGINT:
            print()
        self.girafs.shutdown()
        self.exit_code = 1 if signum == SIGUSR1 else 0
