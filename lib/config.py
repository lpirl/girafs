'''
This modules contains our (customization to the standard library's)
configuration parser.
'''

from configparser import ConfigParser, BasicInterpolation
from getpass import getuser
from logging import error
from pathlib import Path
from socket import gethostname, getfqdn

from .cli import Cli


class Config(ConfigParser):
    '''
    Just like :class:`ConfigParser` of the standard library plus a few
    helpers of ours.
    '''

    DEFAULT_KWARGS = {
        'default_section': 'default',
        'interpolation': BasicInterpolation(),
    }

    @staticmethod
    def get_interpolation_vars(section):
        '''
        Returns a dictionary of variables to interpolate in
        configuration options.
        '''
        return {
            'section_name': section,
            'host_name': gethostname(),
            'fqdn': getfqdn(),
            'user': getuser(),
            'home': Path.home(),
        }

    def __init__(self, *args, **kwargs):
        '''
        Just like :func:`ConfigParser.__init__` but sets a few
        default keyword arguments (if not already in ``kwargs``).
        '''

        for kwkey, kwval in self.DEFAULT_KWARGS.items():
            kwargs.setdefault(kwkey, kwval)

        super().__init__(*args, **kwargs)

    def update_kwars_with_interpolation_vars(self, section, kwargs):
        '''
        Sets default values for ``vars`` in ``kwargs``.

        This is basically used to set our interpolation vars without
        overwriting vars already set.
        '''
        vars_ = kwargs.get('vars', None)
        if vars_ is None:
            vars_ = {}
            kwargs['vars'] = vars_
        for key, value in self.get_interpolation_vars(section).items():
            vars_.setdefault(key, value)

    def get(self, section, *args, **kwargs):
        '''
        Adds a few values for interpolation.
        '''
        self.update_kwars_with_interpolation_vars(section, kwargs)
        return super().get(section, *args, **kwargs)

    def getstr(self, *args, **kwargs):
        '''
        For consistency, an alias for :func:`get`.
        '''
        return self.get(*args, **kwargs)

    def get_or_err(self, section, option, *args, **kwargs):
        '''
        If ``option`` in ``section`` empty or missing, print
        an error and call ``err_func`` if provided, else return the
        corresponding value.
        '''
        value = self.get(section, option, *args, **kwargs)
        if not value:
            error(f'option "{option}" in section "{section}" '
                  'missing or empty')
            Cli.exit_due_to_error()
        return value

    def getstr_or_err(self, section, option, *args, **kwargs):
        ''' alias for :func:`get_or_err` '''
        return self.get_or_err(section, option, *args, **kwargs)

    def getint_or_err(self, section, option, *args, **kwargs):
        '''
        same as :func:`get_or_err`, but converts value to ``int``
        '''
        return int(self.get_or_err(section, option, *args, **kwargs))

    def getboolean_or_err(self, section, option, *args, **kwargs):
        '''
        same as :func:`get_or_err`, but converts value to ``bool``
        '''
        return self._convert_to_boolean(
            self.get_or_err(section, option, *args, **kwargs)
        )
