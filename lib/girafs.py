'''
See :class:`Girafs`.
'''

from logging import debug, info, error, warning
from threading import Event

from .cleaner import Cleaner
from .cli import Cli
from .config import Config
from .notifiers import NotifierRegistry
from .repository import Repository
from .threading import NoRaiseThread



class Girafs:
    '''
    Defines top-level coordination and state.
    '''


    def __init__(self):
        '''
        Minimal initialization of instance variables. The actual fun
        happens in :func:`self.start`.
        '''
        self.cleaner = Cleaner()
        self.config = Config()
        self.shutdown_request = Event()
        ''' Used to send a request to shutdown to threads. '''


    def start(self, config_file_path):
        '''
        Main entry point for setting everything up and running this
        program.

        If this method returns, something went wrong.
        '''
        debug('loading configuration')
        with open(config_file_path) as config_file:
            self.config.read_file(config_file)

        Repository.check_git_config()

        debug('setting up repositories')
        for name in self.config.sections():
            self.start_thread(
                f'setup-{name}',
                self._setup_repository,
                name
            )

        self.shutdown_request.wait()

    def start_thread(self, thread_name, func, *args, **kwargs):
        '''
        Starts a thread and internally keeps track of it so it can be
        properly joined on program exit.
        '''
        thread = NoRaiseThread(
            name=thread_name,
            target=func,
            args=args,
            kwargs=kwargs
        )
        thread.start()
        self.cleaner.add_job(thread.join)


    def _setup_repository(self, name):
        '''
        Initializes a repository (the one configured as ``name``),
        and initiates the setup of the notifiers afterwards.
        '''

        debug(f'initializing repository {name}')
        repository = Repository(name, self.config[name], self.cleaner)

        debug(f'setting up notifiers for {name}')
        self._setup_notifier('pull', name, repository)
        self._setup_notifier('push', name, repository)


    def _setup_notifier(self, action, name, repository):
        '''
        Sets up and runs the ``action`` notifier (i.e., either
        "push" or "pull") if configured.
        '''
        assert action in ('pull', 'push')

        option = f'{action}_notifier'
        value = self.config[name].get(option)
        if not value:
            warning(
                f'no {action} notifier appears to be configured for '
                f'{name} (option "{option}" unset)'
            )
            return

        cls = NotifierRegistry.get(value)
        if cls is None:
            error(f'configured {action} notifier for {name} '
                  f'(configuration "{option} = {value}") not found')
            Cli.exit_due_to_error()

        notifier = cls(self.config[name], repository, self.cleaner,
                       self.shutdown_request)

        self.start_thread(
            f'{action}-{name}',
            notifier.notify_forever,
        )

        self.cleaner.add_job(notifier.shutdown)


    def shutdown(self):
        '''
        Shuts down this girafs instance.

        Can safely be called multiple times for programming convenience.
        '''
        if self.shutdown_request.is_set():
            return
        info(f'shutting down')
        self.shutdown_request.set()
        self.cleaner.do_all_jobs()
