'''
See :class:`WebServer`.
'''

from http.server import ThreadingHTTPServer, BaseHTTPRequestHandler
from logging import error, warning, info, debug
from threading import Lock

from singleton_decorator import singleton

from .cli import Cli
from .threading import NoRaiseThread

@singleton
class WebServer:
    '''
    An abstraction to easily subscribe to HTTP requests.

    Intends to be thread-safe.
    '''

    @staticmethod
    def get_new_route_request_handler():
        '''
        The Web server takes class as request handler. But we want to
        modify the request handler during run time. So we need to
        modify the class. Additionally we of course want different
        request handling per port. Thus we need such a class per port
        we want to serve. From this function, you can retrieve such a
        class.
        '''

        class RouteRequestHandler(BaseHTTPRequestHandler):
            '''
            A request handler where handlers can be attached routes.

            Within this class, we'll call HTTP methods "commands" to be
            in line with the wording in the Python documentation.
            '''

            SUPPORTED_COMMANDS = set(('GET', 'POST'))
            ''' the HTTP commands supported by this request handler '''

            commands_routes_handlers = {}
            '''
            maps from HTTP commands to dicts, which in turn map from
            paths to handler functions
            '''

            @classmethod
            def add_route(cls, command, path, handler):
                '''
                Assigns ``handler`` to ``path`` accessed via
                ``command``.
                '''
                command = command.upper()

                if command not in cls.SUPPORTED_COMMANDS:
                    error(f'method {command} not supported by {cls}')
                    Cli.exit_due_to_error()

                routes_handlers = \
                    cls.commands_routes_handlers.setdefault(command, {})

                if path in routes_handlers:
                    error(f'there is already a handler registered for '
                          f'{command} requests to "{path}" '
                          f'({routes_handlers[path]})')
                    Cli.exit_due_to_error()

                debug(f'adding handler {handler} for {command} '
                      f'requests to "{path}"')
                routes_handlers[path] = handler

            def _do(self):
                '''
                tries to look up handler for current ``self.command``
                and ``self.path``
                '''

                routes_handlers = self.commands_routes_handlers.get(
                    self.command, None
                )
                if routes_handlers is None:
                    warning(f'no routes for {self.command} requests')
                    self.send_response(404)
                    self.end_headers()
                    return

                handler = routes_handlers.get(self.path, None)
                if handler is None:
                    warning(f'no handler for {self.command} request to '
                            f'"{self.path}"')
                    self.send_response(404)
                    self.end_headers()
                    return

                request_body_size = int(
                    self.headers.get('content-length', 0)
                )
                request_body = self.rfile.read(request_body_size).decode()
                return_code = handler(self.headers, request_body)

                if not isinstance(return_code, int):
                    warning(f'handler for {self.command} request to '
                            f'"{self.path}" probably did not return a '
                            f'HTTP status code (got {return_code})')
                    return_code = 500

                self.send_response(return_code)
                self.end_headers()

            def do_GET(self):
                ''' delegates GET requests to :func:`_do` '''
                self._do()

            def do_POST(self):
                ''' delegates POST requests to :func:`_do` '''
                self._do()

        return RouteRequestHandler

    def __init__(self, cleaner):

        self.cleaner = cleaner

        self._handlers = {}
        '''
        Stores the :class:``RouteRequestHandler``s (values) per port
        (keys).
        '''

        self._lock = Lock()

    def _get_handler(self, port):
        '''
        Returns the ``RouteRequestHandler`` for ``port``. If there
        is no such ``RouteRequestHandler``, this method starts a new
        Web server correspondingly.
        '''
        handler = self._handlers.get(port, None)
        if handler is None:

            handler = self.get_new_route_request_handler()

            # Yes, we only want to serve on localhost. For security
            # reasons, use a proper Web server which proxies requests to
            # this server.
            info(f'starting Web server on port {port}')
            server = ThreadingHTTPServer(('localhost', port), handler)
            thread = NoRaiseThread(target=server.serve_forever)
            thread.start()
            self.cleaner.add_job(thread.join)
            self.cleaner.add_job(server.shutdown)
            self._handlers[port] = handler

        return handler

    def subscribe(self, command, path, route_handler, port=8080):
        '''
        Calls ``handler`` when a request is made to ``location``.
        Optionally, you can specify a ``port`` other than 8080.
        '''
        with self._lock:
            request_handler = self._get_handler(port)
            request_handler.add_route(command, path, route_handler)
