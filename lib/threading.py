'''
This module implements some customizations to the standard library's
``threading`` module.
'''

from logging import error
from threading import Thread

from .cli import Cli



class NoRaiseThread(Thread):
    '''
    Just like :class:`threading.Thread`, but exits the program if a
    thread's :func:`run` raises an exception.

    In future, you'd probably solve this using ``threading.excepthook``,
    but this isn't available for our Python version yet (needs >= 3.8).
    '''

    def run(self, *args, **kwargs):
        '''
        Just like :func:`Thread.run`, but exits the program on
        exceptions (except ``SystemExit``, i.e., on normal termination).
        '''
        try:
            super().run(*args, **kwargs)
        except Exception as exception:
            Cli.exit_due_to_error(
                f'exception in thread {self}: {exception}'
            )



class EternalnoRaiseThread(NoRaiseThread):
    '''
    Like :class:`NoRaiseThread`, but also exits the program if the
    thread exits normally.
    '''

    def run(self, *args, **kwargs):
        '''
        Just like :func:`Thread.run`, but exits the program on
        exceptions.
        '''
        try:
            super().run(*args, **kwargs)
        finally:
            Cli.exit_due_to_error(
                f'thread %r exited but should have run forever.' % self
            )
